INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD
QT += concurrent

#contains(DEFINES,FFMPEG_VERSION_V3_4_2){
INCLUDEPATH += ./../../include/V3_4_2/
LIBS += -LE:/SynologyDrive/Core/Qt/SXND/lib/X64/V3_4_2
#}
LIBS += -lavdevice -lavformat -lavutil -lavcodec -lswscale

HEADERS += \
    $$PWD/FQFCore/FQF.h \
    $$PWD/FQFCore/FQFMedia.h \
    $$PWD/FQFCore/FQFMediaPlayer.h \
    $$PWD/FQFCore/FQFMediaPush.h \
    $$PWD/FQFCore/FQFNetworkMonitorThread.h \
    $$PWD/FQFPush/FQFPush.h

SOURCES += \
    $$PWD/FQFCore/FQF.cpp \
    $$PWD/FQFCore/FQFMedia.cpp \
    $$PWD/FQFCore/FQFMediaPlayer.cpp \
    $$PWD/FQFCore/FQFMediaPush.cpp \
    $$PWD/FQFCore/FQFNetworkMonitorThread.cpp \
    $$PWD/FQFPush/FQFPush.cpp
