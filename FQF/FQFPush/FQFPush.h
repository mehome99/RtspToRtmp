#ifndef _FQFPUSH_H_
#define _FQFPUSH_H_

#include "FQF/FQFCore/FQFMedia.h"
#include "FQF/FQFCore/FQFMediaPush.h"
#include "FQF/FQFCore/FQFNetworkMonitorThread.h"

class FQFPush
{
public:
	//构造函数，清空错误信息数组
	FQFPush();
	virtual ~FQFPush();
	void startPush(const char *rtspUrl, const char *rtmpUrl);
    void stopPush();
    bool getState();
    void setRtspConnectType(bool type);
    QString getPushUrl();
    QString getRtmpIp();
    QString getRtmpKey();

protected:
    FQFMedia *media = nullptr;
    FQFMediaPush *push = nullptr;
    FQFNetworkMonitorThread *monitor = nullptr;
};

#endif
