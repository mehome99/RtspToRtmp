#include "FQFPush.h"

FQFPush::FQFPush()
{
	media = new FQFMedia;
	push = new FQFMediaPush(media);
	monitor = new FQFNetworkMonitorThread(media);
}

FQFPush::~FQFPush()
{
	monitor->realseMonitor();
	push->realsePlayer();
}

void FQFPush::startPush(const char *rtspUrl, const char *rtmpUrl)
{
	monitor->setRtspUrl(rtspUrl,StreamOpenMode::PushOnly);
	monitor->setRtmpUrl(rtmpUrl);
	if (monitor->getConnect())
		media->closeStream();
	monitor->startMonitor();
    push->startPlay();
}

void FQFPush::stopPush()
{
    if (monitor->getConnect())
        media->closeStream();
    monitor->stopMonitor();
    push->stopPlay();
}

bool FQFPush::getState()
{
    return monitor->getConnect();
}

void FQFPush::setRtspConnectType(bool type)
{
    media->setRtspTransport(type);
}

QString FQFPush::getPushUrl()
{
    return monitor->getRtmpUrl();
}

QString FQFPush::getRtmpIp()
{
    return monitor->getRtmpIp();
}

QString FQFPush::getRtmpKey()
{
    return monitor->getRtmpKey();
}
