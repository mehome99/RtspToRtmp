#include "FQFMediaPush.h"

FQFMediaPush::FQFMediaPush(FQFMedia *m)
{
	media = m;
}

FQFMediaPush::~FQFMediaPush()
{
}

void FQFMediaPush::setStreamState(bool state)
{
    isPlay = state;
}

bool FQFMediaPush::getStreamState()
{
	return isPlay;
}

void FQFMediaPush::realsePlayer()
{
	isExit = true;
}

void FQFMediaPush::startPlay()
{
	isExit = false;
	this->start();
}

void FQFMediaPush::stopPlay()
{
	isExit = true;
}

void FQFMediaPush::run()
{
	while (!isExit)
	{
		msleep(10);
		if (!media)
		{
			msleep(10);
			continue;
		}
		if (!isPlay)
		{
			msleep(10);
			continue;
		}
		if (!media->getTimeoutState())
		{
			msleep(10);
			continue;
		}
		;
		if (!media->readStream())
		{
			msleep(10);
			continue;
		}
		if (!media->pushPacket())
		{
			msleep(10);
		}
	}
}
