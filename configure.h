#ifndef CONFIGURE_H
#define CONFIGURE_H

#include <QObject>

class Configure : public QObject
{
    Q_OBJECT
private:
    explicit Configure(QObject *parent = nullptr);

public:
    static Configure *getObject();
    QString getRtsp();
    QString getRtmp();
    QString getPostUrl();
    QString getGetUrl();
    QString getGet2Url();
    bool getHlsBoth();
    QString getHlsGroup();
    int getHlsPort();
    bool getHlsHttps();
    bool getRtspConnectType();
    int getPostID();
    QString getGetKey();
    QString getGet2Key();
    void setRtsp(QString url);
    void setRtmp(QString url);
    void setPostUrl(QString url);
    void setGetUrl(QString url);
    void setGet2Url(QString url);
    void setHlsBoth(bool ok);
    void setHlsGroup(QString group);
    void setHlsPort(int port);
    void setHlsHttps(bool ok);
    void setRtspConnectType(bool type);
    void setPostID(int id);
    void setGetKey(QString key);
    void setGet2Key(QString key);


protected:

    void initSettingFile();
//    bool checkStrin

};

#endif // CONFIGURE_H
