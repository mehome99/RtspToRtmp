# RtspToRtmp

#### 介绍
1. Qt Rtsp 推流客户端
2. 支持TCP/UDP接入RTSP流媒体
3. 多码流无损转换RTMP
4. 可自动计算、提交HLS地址，支持对地址自定义

![软件界面](show1.png)

#### 已知问题
1. 连续点击提交按钮会导致软件卡死（后台阻塞等待Web服务器结果）

#### 使用说明

1. 输入的RTSP流，支持以“rtsp://”开头的网络摄像头地址，同时支持以TCP或UDP方式打开，请在打开前选择打开方式。
2. 输出的RTMP流，支持以“rtmp://”开头的流媒体服务器地址，不需要填写流名称，软件会自动生成，只需要填写服务器地址，RTMP组即可，当服务器地址为“www.albert.com”，组为“hls”，则地址应当如下：“rtmp://www.albert.com/hls”。
3. 如服务器配置切片参数，请填写播放的端口，此时生成的hls流可播放，否则只支持rtmp流。
4. 当网络中断，会自动进行重连，如遇流名称冲突，会自动更换流名称。
5. 支持自动和手动提交新的hls地址到WEB服务器，请填写对应的API地址，会提交一条JSON数据或普通Get数据到对应接口，包含设置的id及新的地址。
   1. 当id输入0时提交的数据示例如下：
      
      1. ```
         “{"stream_id":0;"hls_url":"http://www.albert.com:8000/hls/aEgd.m3u8";"rtmp_url":"rtmp://www.albert.com/hls/aEgd"}”
         ```
      
   2. 当采用GET方式提交时，Key可配置，当key输入url时提交的数据示例如下：
      
      1. ```
         “http://geturl?hls_url=http://www.albert.com:8000/hls/aEgd.m3u8”
         ```
      2. ```
         “http://geturl?rtmp_url=rtmp://www.albert.com/hls/aEgd”
         ```
      
         

#### 软件架构
基于FFmpeg3.4.2


#### 更新说明[V2.0]

##### 新增：
1.  HLS开关
2.  HLS组自定义
3.  HLS https选择

##### 优化：
1.	显示优化

#### 更新说明[V1.3]

##### 新增：
1.	POST提交指定ID
2.	GET提交地址

##### 优化：
1.	稳定性提高

#### 更新说明[V1.2]

##### 新增：
1.	POST提交地址

##### 优化：
1.	断线自动重连

#### 更新说明[V1.1]

##### 新增：
1.	RTSP连接支持TCP、UDP切换

##### 优化：
1.	BUG 修复

#### 更新说明[V1.0]

##### 新增：
1.	采用FQFLib_Push版实现Rtsp码流解析
2.	FFmpeg选择V3.4.2
3.	选用管理界面设定参数，方便修改

