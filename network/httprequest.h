#ifndef HTTPREQUEST_H
#define HTTPREQUEST_H

#include <QObject>
class QTimer;
class QEventLoop;

class HttpRequest : public QObject
{
    Q_OBJECT
private:
    explicit HttpRequest(QObject *parent = nullptr);

public:
    static HttpRequest *getObject();
    //提交新地址
    bool uploadHlsUrl(QString httpUrl, QString rtmpUrl, QString hlsUrl, int id);
    //提交新地址
    bool uploadHlsUrl2(QString httpUrl, QString hlsUrl, QString key);

protected:
    QTimer *uploadTimer = nullptr;          //文件服务器提交等待定时器
    QEventLoop *loop = nullptr;             //阻塞等待
};

#endif // HTTPREQUEST_H
