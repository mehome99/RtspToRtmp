#-------------------------------------------------
#
# Project created by QtCreator 2018-04-28T09:22:03
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RtspToRtmp
TEMPLATE = app
#contains(DEFINES,FFMPEG_VERSION_V3_4_2){
DESTDIR = E:/SynologyDrive/Core/Qt/SXND/bin/X64/V3_4_2
#}

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
include(FQF/FQF.pri)
include(network/network.pri)

SOURCES += \
        configure.cpp \
        main.cpp \
        rtmppush.cpp

HEADERS += \
        configure.h \
        rtmppush.h

FORMS += \
        rtmppush.ui

RESOURCES += \
    images.qrc
